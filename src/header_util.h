/*
 * Copyright 2018 Takuya Kitano
 */
#ifndef HEADER_UTIL_H_  // include guard
#define HEADER_UTIL_H_

#include <linkedList.h>   // LinkedList

extern int MAX_MESSAGE_LEN;
extern int MAX_METHOD_LEN;

typedef struct httpRequest {
  char *method;
  char *host;
  char *path;
  char *query;
  LinkedList *headers;
  char *body;
  int body_file;
  int body_file_len;
} httpRequest;

typedef struct httpResponse {
  int status;
  LinkedList *headers;
  char *body;
  int body_file;
  int body_file_len;
} httpResponse;


/*
 * ############################
 * ###   http client util   ###
 * ############################
 */
httpRequest *initHttpRequest(
  char *method,
  char *host,
  char *path,
  char *query,
  LinkedList *headers,
  char *body,
  int body_file,
  int body_file_len);

void *deleteHttpRequest(httpRequest *http_request);

char *createRequestMsg(httpRequest http_request);

httpResponse *parseResponseHdr(char *header);

int getHdrStat(LinkedList *headers);

/*
 * ############################
 * ###   http server util   ###
 * ############################
 */
httpRequest *parseRequestHdr(char *header);

char *getHdrMethod(LinkedList *headers);

char *getHdrPath(LinkedList *headers);

char *getHdrQuery(LinkedList *headers);

httpResponse *initHttpResponse(
  int status,
  LinkedList *headers,
  char *body,
  int body_file,
  int body_file_len);

void *deleteHttpResponse(httpResponse *http_response);

char *createResponseMsg(httpResponse http_response);

/*
 * #######################
 * ###   common util   ###
 * #######################
 */
char *copyStr(char *str);

char *appendStr(char *to, int to_alloc_total, char *str);

int getHdrVal(LinkedList *headers, char *header_key);

char *getHdrStr(LinkedList *headers, char *header_key);
/*
char *getBdy(char *body, int content_length);
*/
#endif  // HEADER_UTIL_H_
