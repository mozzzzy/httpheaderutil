/*
 * Copyright 2018 Takuya Kitano
 */
#include <stdio.h>    // sprintf
#include <stdlib.h>   // malloc
#include <string.h>   // strlen
#include <linkedList.h>

#include "header_util.h"    // httpRequest, httpResponse

int MAX_MESSAGE_LEN = 1024;
int MAX_METHOD_LEN = 10;

/*
 * ##########################
 * ###  http client util  ###
 * ##########################
 */
/*
 * initHttpRequest
 *
 * [Description]
 * Getting each information of http request,
 * This function create httpRequest structure.
 *
 * [Parameter]
 * - char *method
 * - char *host
 * - char *path
 * - char *query
 * - LinkedList *headers
 * - char *body
 * - int body_file
 * - int body_file_len
 *
 * [Return Value]
 * - httpRequest *http_request
 */
httpRequest *initHttpRequest(
  char *method,
  char *host,
  char *path,
  char *query,
  LinkedList *headers,
  char *body,
  int body_file,
  int body_file_len) {
  httpRequest *http_request = (httpRequest *) malloc(sizeof(httpRequest));
  http_request->method = copyStr(method);
  http_request->host = copyStr(host);
  http_request->path = copyStr(path);
  http_request->query = copyStr(query);
  http_request->headers = headers;
  http_request->body = copyStr(body);
  if (body_file > 0) {
    http_request->body_file = body_file;
  } else {
    http_request->body_file = -1;
  }
  if (body_file_len > 0) {
    http_request->body_file_len = body_file_len;
  } else {
    http_request->body_file_len = -1;
  }
  return http_request;
}


/*
 * deleteHttpRequest
 *
 * [Description]
 * Getting pointer of httpResponse, this function free each elements.
 *
 * [Parameter]
 * - httpRequest *http_request
 *
 * [Return Value]
 * - No return value
 */
void *deleteHttpRequest(httpRequest *http_request) {
  if (!http_request) {
    return;
  }

  if (http_request->method) {
    free(http_request->method);
  }
  if (http_request->host) {
    free(http_request->host);
  }
  if (http_request->path) {
    free(http_request->path);
  }
  if (http_request->query) {
    free(http_request->query);
  }
  if (http_request->headers) {
    deleteLinkedList(http_request->headers);
  }
  if (http_request->body) {
    free(http_request->body);
  }
  if (http_request) {
    free(http_request);
  }
}


/*
 * createRequestMsg
 *
 * [Description]
 * Getting header parameters, this function creates http request string.
 * NOTE : We should execute `free` when we finish using returned msg_str.
 *
 * [Parameter]
 * - httpRequest http_request : HTTP request information.
 *
 * [Return Value]
 * - If success : char *msg_str
 * - Otherwise  : NULL
 */
char *createRequestMsg(httpRequest http_request) {
  char message[MAX_MESSAGE_LEN];
  message[0] = '\0';

  if (http_request.method == NULL || http_request.host == NULL
    || http_request.path == NULL) {
    return NULL;
  }

  // append method
  appendStr(message, MAX_MESSAGE_LEN, http_request.method);
  appendStr(message, MAX_MESSAGE_LEN, " ");

  // append path
  appendStr(message, MAX_MESSAGE_LEN, http_request.path);

  // append query
  appendStr(message, MAX_MESSAGE_LEN, http_request.query);

  // append protocol
  appendStr(message, MAX_MESSAGE_LEN, " HTTP/1.1\r\n");

  // append "HOST: "
  appendStr(message, MAX_MESSAGE_LEN, "HOST: ");

  // append host
  appendStr(message, MAX_MESSAGE_LEN, http_request.host);

  // append "\r\n"
  appendStr(message, MAX_MESSAGE_LEN, "\r\n");

  // append additional header
  if (http_request.headers != NULL) {
    int header_num = http_request.headers->data_num;
    int header_count;
    for (header_count = 0; header_count < header_num; header_count ++) {
      LinkedListData *header = getData(http_request.headers, header_count);
      char *header_str = header->data;
      appendStr(message, MAX_MESSAGE_LEN, header_str);
      appendStr(message, MAX_MESSAGE_LEN, "\r\n");
    }
  }

  // append Content-Length header
  int content_length = 0;
  if (http_request.body != NULL && strlen(http_request.body) > 0) {
    content_length += strlen(http_request.body);
  }
  // if body file is specified
  if (http_request.body_file >= 0 && http_request.body_file_len > 0) {
    content_length += http_request.body_file_len;
  }
  if (content_length > 0) {
    char content_length_header[MAX_MESSAGE_LEN];
    snprintf(content_length_header, MAX_MESSAGE_LEN,
      "Content-Length: %d\r\n", content_length);

    appendStr(message, MAX_MESSAGE_LEN, content_length_header);
  }

  // append "\r\n"
  appendStr(message, MAX_MESSAGE_LEN, "\r\n");

  // append body
  if (http_request.body != NULL && strlen(http_request.body) > 0) {
    appendStr(message, MAX_MESSAGE_LEN, http_request.body);
  }

  char *msg_str = copyStr(message);
  return msg_str;
}


/*
 * parseResponseHdr
 *
 * [Description]
 * Getting pointer of header string,
 * this function plugs each header string to http_response.headers.
 * NOTE : We should execute `free` to each element of http_response->headers,
 *        and we should execute `delete` when we finish using retuned
 *        httpResponse *http_response.
 *
 * [Parameter]
 * - char *header : header string.
 *
 * [Return Value]
 * - httpResponse *http_response
 */
httpResponse *parseResponseHdr(char *header) {
  //httpResponse *http_response = (httpResponse *) malloc(sizeof(httpResponse));
  httpResponse *http_response = initHttpResponse(
    0,
    NULL,
    NULL,
    0,
    0
  );

  // create new LinkedList
  LinkedList *headers = initLinkedList();

  // find each header's end
  char *each_header_end;
  int each_header_len;
  while ((each_header_end = strstr(header, "\r\n")) != NULL) {
    each_header_len = each_header_end - header;
    char *each_header =
      (char *) malloc(sizeof(char) * (each_header_len + 1));
    strncpy(each_header, header, each_header_len);
    *(each_header + each_header_len) = '\0';

    addData(headers, each_header);
    header = each_header_end + 2;
  }

  http_response->headers = headers;

  // get http status
  http_response->status = getHdrStat(http_response->headers);

  return http_response;
}


/*
 * getHdrStat
 *
 * [Description]
 * Getting a vector that contains each http header,
 * this function returns http status code.
 *
 * [Parameter]
 * - std::vector<char *> headers : Header strings.
 *
 * [Return Value]
 * - If get successfully : status code
 * - Otherwise           : -1
 */
int getHdrStat(LinkedList *headers) {
  // check header length is not 0.
  int header_length = headers->data_num;
  if (header_length > 0) {
    LinkedListData *first_hdr = getData(headers, 0);

    if (first_hdr != NULL) {
      // copy first line of header
      char *first_hdr_str =
        (char *) malloc(sizeof(char) * strlen(first_hdr->data));
      snprintf(first_hdr_str, strlen(first_hdr->data), first_hdr->data);

      // get status code start
      // status code is next of ' '. So increment +1
      char *status_code_start = strchr(first_hdr_str, ' ') + 1;
      // get status code end
      char *status_code_end = strchr(status_code_start, ' ');
      // get status code string
      char *status_code_str = (char *) malloc(sizeof(char) * 4);
      strncpy(status_code_str, status_code_start, 3);

      char *status_code_str_end = status_code_str + 3;
      *status_code_str_end = '\0';
      // convert char* to int
      int status_code = atoi(status_code_str);

      free(first_hdr_str);
      free(status_code_str);

      return status_code;
    }
  }
  return -1;
}


/*
 * ##########################
 * ###  http server util  ###
 * ##########################
 */
/*
 * parseRequestHdr
 *
 * [Description]
 * Getting header string, this function plugs each header string to http_request.headers.
 * NOTE : We should execute `free` to each element of http_request->headers,
 *        and should execute `delete` when we finish using retuned httpRequest *http_request.
 *
 * [Parameter]
 * - char *header : header string.
 *
 * [Return Value]
 * - httpRequest *http_request
 */
httpRequest *parseRequestHdr(char *header) {
  httpRequest *http_request = initHttpRequest(
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    0,
    0
  );
  http_request->headers = initLinkedList();

  // find each header's end
  char *each_header_end;
  int each_header_len;
  while ((each_header_end = strstr(header, "\r\n")) != NULL) {
    each_header_len = each_header_end - header;
    char *each_header =
      (char *) malloc(sizeof(char) * (each_header_len + 1));
    strncpy(each_header, header, each_header_len);
    *(each_header + each_header_len) = '\0';

    addData(http_request->headers, each_header);
    header = each_header_end + 2;
  }

  // get http method
  http_request->method = getHdrMethod(http_request->headers);

  // get HOST
  http_request->host = getHdrStr(http_request->headers, "HOST");
  if (http_request->host == NULL) {
    http_request->host = getHdrStr(http_request->headers, "Host");
  }

  // get path
  http_request->path = getHdrPath(http_request->headers);

  // get query
  http_request->query = getHdrQuery(http_request->headers);
  return http_request;
}


/*
 * getHdrMethod
 *
 * [Description]
 * Getting each http headers,
 * this function returns http request method.
 *
 * [Parameter]
 * - LinkedList *headers : Header strings.
 *
 * [Return Value]
 * - If get successfully : pointer of method string
 * - Otherwise           : NULL
 */
char *getHdrMethod(LinkedList *headers) {
  if (headers == NULL || headers->data_num <= 0) {
    return NULL;
  }
  // check header length is not 0.
  int header_length = headers->data_num;

  // copy first line of header
  char *first_hdr = headers->first_data->data;
  // get method end
  char *method_end = strchr(first_hdr, ' ');
  // get method string
  char *method_str =
    (char *) malloc(sizeof(char) * (method_end - first_hdr));
  strncpy(method_str, first_hdr, method_end - first_hdr);

  char *method_str_end = method_str + (method_end - first_hdr);
  *method_str_end = '\0';

  return method_str;
}


/*
 * getHdrPath
 *
 * [Description]
 * Getting LinkedList that contains each http header,
 * this function returns path.
 * NOTE : We should execute `free` if we finish using returned path.
 *
 * [Parameter]
 * - LinkedList *headers : Header strings.
 *
 * [Return Value]
 * - If get successfully : char *path
 * - Otherwise           : NULL
 */
char *getHdrPath(LinkedList *headers) {
  if (headers == NULL || headers->data_num <= 0) {
    return NULL;
  }

  int header_length = headers->data_num;

  // copy first line of header
  LinkedListData *first_hdr = getData(headers, 0);

  // path start
  char *path_start = strchr(first_hdr->data, ' ') + 1;
  // get path end
  char *path_end = strchr(path_start, ' ');
  // get path string
  char *path_str =
    (char *) malloc(sizeof(char) * (path_end - path_start) + 1);
  strncpy(path_str, path_start, (path_end - path_start));

  char *path_str_end = path_str + (path_end - path_start);
  *path_str_end = '\0';

  char *query_start;
  if ((query_start = strstr(path_str, "?")) != NULL) {
    *query_start = '\0';
  }

  return path_str;
}


/*
 * getHdrQuery
 *
 * [Description]
 * Getting LinkedList that contains each http header,
 * this function returns query string.
 * NOTE : We should execute `free` if we finish using returned query.
 *
 * [Parameter]
 * - LinkedList *headers : Header strings.
 *
 * [Return Value]
 * - If get successfully : char *query
 * - Otherwise           : NULL
 */
char *getHdrQuery(LinkedList *headers) {
  if (headers == NULL || headers->data_num <= 0) {
    return NULL;
  }

  // copy first line of header
  LinkedListData *first_hdr = getData(headers, 0);
  // path code start
  char *path_start = strchr(first_hdr->data, ' ') + 1;
  // get path end
  char *path_end = strchr(path_start, ' ');
  // get path string
  char *path_str =
    (char *) malloc(sizeof(char) * (path_end - path_start) + 1);
  strncpy(path_str, path_start, (path_end - path_start));

  char *path_str_end = path_str + (path_end - path_start);
  *path_str_end = '\0';

  char *query_start;
  char *query_str;
  if ((query_start = strchr(path_str, '?')) != NULL) {
    query_str =
      (char *) malloc(sizeof(char) * (path_str_end - query_start + 1));
    strncpy(query_str, query_start, (path_str_end - query_start));
    *(query_str + (path_str_end - query_start)) = '\0';
  } else {
    free(path_str);
    return NULL;
  }

  free(path_str);

  return query_str;
}


/*
 * initHttpResponse
 *
 * [Description]
 * Getting each information of http response,
 * This function create httpResponse structure.
 *
 * [Parameter]
 * - int status;
 * - LinkedList *headers;
 * - char *body;
 * - int body_file;
 * - int body_file_len;
 *
 * [Return Value]
 * - httpResponse *http_response
 */
httpResponse *initHttpResponse(
  int status,
  LinkedList *headers,
  char *body,
  int body_file,
  int body_file_len) {
  httpResponse *http_response = (httpResponse *) malloc(sizeof(httpResponse));
  http_response->status = status;
  http_response->headers = headers;
  http_response->body = copyStr(body);
  if (body_file > 0) {
    http_response->body_file = body_file;
  } else {
    http_response->body_file = -1;
  }
  if (body_file_len > 0) {
    http_response->body_file_len = body_file_len;
  } else {
    http_response->body_file_len = -1;
  }
  return http_response;
}


/*
 * deleteHttpResponse
 *
 * [Description]
 * Getting pointer of httpResponse, this function free each elements.
 *
 * [Parameter]
 * - httpResponse *http_response
 *
 * [Return Value]
 * - No return value
 */
void *deleteHttpResponse(httpResponse *http_response) {
  if (!http_response) {
    return;
  }

  if (http_response->headers) {
    deleteLinkedList(http_response->headers);
  }
  if (http_response->body) {
    free(http_response->body);
  }
  if (http_response) {
    free(http_response);
  }
}


/*
 * createResponseMsg
 *
 * [Description]
 * Getting response parameters, this function creates http response string.
 * NOTE : We should execute `free` when we finish using returned msg_str.
 *
 * [Parameter]
 * - httpResponse http_response : HTTP response information.
 *
 * [Return Value]
 * - If success : char *msg_str
 * - Otherwise  : NULL
 */
char *createResponseMsg(httpResponse http_response) {
  char message[MAX_MESSAGE_LEN];
  memset(message, 0, sizeof(message));

  // append http version
  snprintf(message, sizeof(message), "HTTP/1.1");

  // append status
  char status_line[MAX_MESSAGE_LEN];
  switch (http_response.status) {
    case 200 :
      snprintf(status_line, MAX_MESSAGE_LEN, " %d OK\r\n", http_response.status);
      appendStr(message, MAX_MESSAGE_LEN, status_line);
      break;
    case 404 :
      snprintf(status_line, MAX_MESSAGE_LEN, " %d Not Found\r\n", http_response.status);
      appendStr(message, MAX_MESSAGE_LEN, status_line);
      break;
    case 500 :
      snprintf(status_line, MAX_MESSAGE_LEN, " %d Internal Error\r\n", http_response.status);
      break;
    default :
      // unsupported status code
      return NULL;
  }

  // calc content length
  int content_length = 0;
  if (http_response.body != NULL) {
    content_length += strlen(http_response.body);
  }
  if (http_response.body_file > 0 && http_response.body_file_len > 0) {
    content_length += http_response.body_file_len;
  }
  // append Content-Length
  char content_length_line[MAX_MESSAGE_LEN];
  snprintf(content_length_line, MAX_MESSAGE_LEN, "Content-Length: %d\r\n", content_length);
  appendStr(message, MAX_MESSAGE_LEN, content_length_line);

  // append additional header
  LinkedList *headers = http_response.headers;
  if (headers == NULL || headers->data_num <= 0) {
    return NULL;
  }
  int hdr_size = headers->data_num;
  int hdr_cnt = 0;
  while (hdr_cnt < hdr_size) {
    LinkedListData *header = getData(headers, hdr_cnt);

    if (header != NULL && header->data != NULL && strlen(header->data) > 0) {
      appendStr(message, MAX_MESSAGE_LEN, header->data);
      appendStr(message, MAX_MESSAGE_LEN, "\r\n");
    }
    hdr_cnt++;
  }

  // append \r\n
  appendStr(message, MAX_MESSAGE_LEN, "\r\n");

  // append body
  if (http_response.body != NULL && strlen(http_response.body) > 0) {
    appendStr(message, MAX_MESSAGE_LEN, http_response.body);
  }

  char *msg_str = copyStr(message);
  return msg_str;
}


/*
 * #####################
 * ###  common util  ###
 * #####################
 */
/*
 * copyStr
 *
 * [Description]
 * Getting pointer of string, this function allocate memory to copy,
 * and copy string.
 *
 * [Parameter]
 * char *str
 *
 * [Return Value]
 * If copy successfully : char *copy
 * Otherwise            : NULL
 */
char *copyStr(char *str) {
  char *copy;
  if(str != NULL && strlen(str) > 0) {
    unsigned int str_len = strlen(str);
    copy = (char *) malloc(sizeof(char) * (str_len + 1));
    snprintf(copy, str_len + 1, str);
  } else {
    copy = NULL;
  }
  return copy;
}


/*
 * appendStr
 *
 * [Description]
 *
 * [Parameter]
 *
 * [Return Value]
 */
char *appendStr(char *to, int to_alloc_total, char *str) {
  if (to != NULL && str != NULL) {
    int to_len = strlen(to);
    int str_len = strlen(str);
    if (to_alloc_total >= to_len + str_len) {
      // append str
      snprintf(to + to_len, to_alloc_total - to_len, str);
    }
  }
  return to;
}


/*
 * getHdrVal
 *
 * [Description]
 * Getting a LinkedList that contains each http header, and a header key string,
 * this function returns specified header's value.
 *
 * [Parameter]
 * - LinkedList *headers : Header strings.
 * - char *header_key    : Header key that you want to get value.
 *
 * [Return Value]
 * - If get successfully : header's value
 * - Otherwise           : -1
 */
int getHdrVal(LinkedList *headers, char *header_key) {
  char *header_str = getHdrStr(headers, header_key);
  if (header_str == NULL) {
    return -1;
  }
  int header_val = atoi(header_str);

  free(header_str);
  return header_val;
}


/*
 * getHdrStr
 *
 * [Description]
 * Getting LinkedList that contain each http header, and a header key string,
 * this function returns specified header's string.
 * NOTE : We should execute `free` when we finish using returned header_str.
 *
 * [Parameter]
 * - LinkedList *headers : Header strings.
 * - char *header_key    : Header key that you want to get value.
 *
 * [Return Value]
 * - If get successfully : header's string
 * - Otherwise           : NULL
 */
char *getHdrStr(LinkedList *headers, char *header_key) {
  if (headers == NULL || headers->data_num <= 0) {
    return NULL;
  }

  char *header_str;
  int header_length = headers->data_num;
  int hdr_cnt;
  for (hdr_cnt = 0; hdr_cnt < header_length; hdr_cnt ++) {
    LinkedListData *header = getData(headers, hdr_cnt);
    char *chk_hdr;
    if ((chk_hdr = strstr(header->data, header_key)) != NULL) {
      chk_hdr = chk_hdr + strlen(header_key) + 2;   // 2 = strlen(": ")
      header_str = (char *) malloc(strlen(chk_hdr) + 1);
      snprintf(header_str, strlen(chk_hdr) + 1, "%s\0", chk_hdr);
      return header_str;
    }
  }

  return NULL;
}


/*
 * getBdy
 *
 * [Description]
 * Getting ponter of charactor and content_length,
 * this function returns pointer of char
 * whose value is body data with '\0' at last of body string.
 * NOTE : We should execute `free` when we finish using returned char *body_str.
 *
 * [Parameter]
 * - char *body         : body data without '\0'
 * - int content_length : content length
 *
 * [Return Value]
 * - If parse successfully : char *body_str
 * - Otherwise             : NULL
 */
/*
char *getBdy(char *body, int content_length) {
  if (content_length < 0) {
    return NULL;
  }

  char *body_str =
    reinterpret_cast<char *>(malloc(sizeof(char) * (content_length + 1)));
  strncpy(body_str, body, content_length);
  char *body_end = body_str + content_length;
  *body_end = '\0';
  return body_str;
}
*/
