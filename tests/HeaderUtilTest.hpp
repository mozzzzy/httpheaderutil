/*
 * Copyright 2018 Takuya Kitano
 */
#include <cppunit/extensions/HelperMacros.h>  // CPPUNIT_TEST_SUITE, CPPUNIT_TEST ...

#include <string.h>    // strcmp
#include <stdlib.h>    // malloc
#include <unistd.h>    // write, close
#include <sys/stat.h>  // S_IREAD, S_IWRITE
#include <fcntl.h>     // open, O_RDWR, O_TRUNC, O_CREAT
#include <errno.h>     // errno

extern "C"
{
#include <linkedList.h>
#include <header_util.h>
}


class HeaderUtilTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(HeaderUtilTest);

  // register each test
  /*
   * ############################
   * ###   http client util   ###
   * ############################
   */
 // createRequestMsg func : POST request
  // F  : fqdn
  // P  : path
  // Q  : query
  // H  : other header
  // B  : body data
  // BF : body data file
  CPPUNIT_TEST(test_createRequestMsg_post_noF_noP_noQ_noH_noB);
  CPPUNIT_TEST(test_createRequestMsg_post_F_noP_noQ_noH_noB);
  CPPUNIT_TEST(test_createRequestMsg_post_F_P_noQ_noH_noB);
  CPPUNIT_TEST(test_createRequestMsg_post_F_P_Q_noH_noB);
  CPPUNIT_TEST(test_createRequestMsg_post_F_P_Q_H_noB);
  CPPUNIT_TEST(test_createRequestMsg_post_F_P_Q_H_B);
  CPPUNIT_TEST(test_createRequestMsg_post_F_P_Q_H_BF);

  // parseResponseHdr func
  CPPUNIT_TEST(test_parseResponseHdr_200);

  // getHdrStat func : 200 OK
  CPPUNIT_TEST(test_getHdrStat_200);
  // getHdrStat func : 404 NOT FOUND
  CPPUNIT_TEST(test_getHdrStat_404);

  /*
   * ############################
   * ###   http server util   ###
   * ############################
   */
  // parseRequestHdr func
  CPPUNIT_TEST(test_parseRequestHdr_get_noQ);
  CPPUNIT_TEST(test_parseRequestHdr_get_Q);

  // getHdrMethod func
  CPPUNIT_TEST(test_getHdrMethod);

  // getHdrPath func
  CPPUNIT_TEST(test_getHdrPath_noQ);
  CPPUNIT_TEST(test_getHdrPath_Q);

  // getHdrQuery func
  CPPUNIT_TEST(test_getHdrQuery_noQ);
  CPPUNIT_TEST(test_getHdrQuery_Q);

  // createResponseMsg func
  CPPUNIT_TEST(test_createResponseMsg_noB);
  CPPUNIT_TEST(test_createResponseMsg_B);
  CPPUNIT_TEST(test_createResponseMsg_BF);

  /*
   * #######################
   * ###   common util   ###
   * #######################
   */
  // getHdrVal func


  // getHdrStr func


  // getBdy func


  CPPUNIT_TEST_SUITE_END();

 public:
  void test_createRequestMsg_post_noF_noP_noQ_noH_noB();
  void test_createRequestMsg_post_F_noP_noQ_noH_noB();
  void test_createRequestMsg_post_F_P_noQ_noH_noB();
  void test_createRequestMsg_post_F_P_Q_noH_noB();
  void test_createRequestMsg_post_F_P_Q_H_noB();
  void test_createRequestMsg_post_F_P_Q_H_B();
  void test_createRequestMsg_post_F_P_Q_H_BF();

  void test_parseResponseHdr_200();

  void test_getHdrStat_200();
  void test_getHdrStat_404();

  void test_parseRequestHdr_get_noQ();
  void test_parseRequestHdr_get_Q();

  void test_getHdrMethod();

  void test_getHdrPath_noQ();
  void test_getHdrPath_Q();

  void test_getHdrQuery_noQ();
  void test_getHdrQuery_Q();

  void test_createResponseMsg_noB();
  void test_createResponseMsg_B();
  void test_createResponseMsg_BF();

  static const int BUF_SIZE = 1024;
};


void HeaderUtilTest::test_createRequestMsg_post_noF_noP_noQ_noH_noB() {
  char method[] = "POST";

  httpRequest *http_request = initHttpRequest(
    method,
    NULL,   // host
    NULL,   // path
    NULL,   // query
    NULL,   // headers
    NULL,   // body
    -1,     // body_file
    -1);    // body_file_len
  CPPUNIT_ASSERT(http_request != NULL);
  CPPUNIT_ASSERT(!strcmp(http_request->method, method));
  CPPUNIT_ASSERT(http_request->host == NULL);
  CPPUNIT_ASSERT(http_request->path == NULL);
  CPPUNIT_ASSERT(http_request->query == NULL);
  CPPUNIT_ASSERT(http_request->headers == NULL);
  CPPUNIT_ASSERT(http_request->body == NULL);
  CPPUNIT_ASSERT(http_request->body_file == -1);
  CPPUNIT_ASSERT(http_request->body_file_len == -1);

  char *header_msg = createRequestMsg(*http_request);

  CPPUNIT_ASSERT(header_msg == NULL);

  deleteHttpRequest(http_request);
}


void HeaderUtilTest::test_createRequestMsg_post_F_noP_noQ_noH_noB() {
  char method[] = "POST";
  char fqdn[] = "localhost";

  httpRequest *http_request = initHttpRequest(
    method,
    fqdn,
    NULL,   // path
    NULL,   // query
    NULL,   // headers
    NULL,   // body
    -1,     // body_file
    -1);    // body_file_len
  CPPUNIT_ASSERT(http_request != NULL);
  CPPUNIT_ASSERT(!strcmp(http_request->method, method));
  CPPUNIT_ASSERT(!strcmp(http_request->host, fqdn));
  CPPUNIT_ASSERT(http_request->path == NULL);
  CPPUNIT_ASSERT(http_request->query == NULL);
  CPPUNIT_ASSERT(http_request->headers == NULL);
  CPPUNIT_ASSERT(http_request->body == NULL);
  CPPUNIT_ASSERT(http_request->body_file == -1);
  CPPUNIT_ASSERT(http_request->body_file_len == -1);

  char *header_msg = createRequestMsg(*http_request);

  CPPUNIT_ASSERT(header_msg == NULL);

  deleteHttpRequest(http_request);
}


void HeaderUtilTest::test_createRequestMsg_post_F_P_noQ_noH_noB() {
  char method[] = "POST";
  char fqdn[] = "localhost";
  char path[] = "/index.html";

  httpRequest *http_request = initHttpRequest(
    method,
    fqdn,
    path,
    NULL,   // query
    NULL,   // headers
    NULL,   // body
    -1,     // body_file
    -1);    // body_file_len
  CPPUNIT_ASSERT(http_request != NULL);
  CPPUNIT_ASSERT(!strcmp(http_request->method, method));
  CPPUNIT_ASSERT(!strcmp(http_request->host, fqdn));
  CPPUNIT_ASSERT(!strcmp(http_request->path, path));
  CPPUNIT_ASSERT(http_request->query == NULL);
  CPPUNIT_ASSERT(http_request->headers == NULL);
  CPPUNIT_ASSERT(http_request->body == NULL);
  CPPUNIT_ASSERT(http_request->body_file == -1);
  CPPUNIT_ASSERT(http_request->body_file_len == -1);

  char *header_msg = createRequestMsg(*http_request);

  char expected[] =
    "POST /index.html HTTP/1.1\r\n"
    "HOST: localhost\r\n"
    "\r\n";

  CPPUNIT_ASSERT(strcmp(expected, header_msg) == 0);

  deleteHttpRequest(http_request);
}


void HeaderUtilTest::test_createRequestMsg_post_F_P_Q_noH_noB() {
  char method[] = "POST";
  char fqdn[] = "localhost";
  char path[] = "/index.html";
  char query[] = "?query=this_is_query";

  httpRequest *http_request = initHttpRequest(
    method,
    fqdn,
    path,
    query,
    NULL,   // headers
    NULL,   // body
    -1,     // body_file
    -1);    // body_file_len
  CPPUNIT_ASSERT(http_request != NULL);
  CPPUNIT_ASSERT(!strcmp(http_request->method, method));
  CPPUNIT_ASSERT(!strcmp(http_request->host, fqdn));
  CPPUNIT_ASSERT(!strcmp(http_request->path, path));
  CPPUNIT_ASSERT(!strcmp(http_request->query, query));
  CPPUNIT_ASSERT(http_request->headers == NULL);
  CPPUNIT_ASSERT(http_request->body == NULL);
  CPPUNIT_ASSERT(http_request->body_file == -1);
  CPPUNIT_ASSERT(http_request->body_file_len == -1);

  char *header_msg = createRequestMsg(*http_request);

  char expected[] =
    "POST /index.html?query=this_is_query HTTP/1.1\r\n"
    "HOST: localhost\r\n"
    "\r\n";

  CPPUNIT_ASSERT(strcmp(expected, header_msg) == 0);

  deleteHttpRequest(http_request);
}


void HeaderUtilTest::test_createRequestMsg_post_F_P_Q_H_noB() {
  char method[] = "POST";
  char fqdn[] = "localhost";
  char path[] = "/index.html";
  char query[] = "?query=this_is_query";
  char user_agent[] = "User-Agent: sample_http_client";
  char accept[] = "Accept: */*";

  LinkedList *headers = initLinkedList();

  char *ua = copyStr(user_agent);
  addData(headers, ua);

  char *acc = copyStr(accept);
  addData(headers, acc);

  httpRequest *http_request = initHttpRequest(
    method,
    fqdn,
    path,
    query,
    headers,
    NULL,   // body
    -1,     // body_file
    -1);    // body_file_len
  CPPUNIT_ASSERT(http_request != NULL);
  CPPUNIT_ASSERT(!strcmp(http_request->method, method));
  CPPUNIT_ASSERT(!strcmp(http_request->host, fqdn));
  CPPUNIT_ASSERT(!strcmp(http_request->path, path));
  CPPUNIT_ASSERT(!strcmp(http_request->query, query));
  CPPUNIT_ASSERT(http_request->headers->data_num == 2);
  CPPUNIT_ASSERT(http_request->body == NULL);
  CPPUNIT_ASSERT(http_request->body_file == -1);
  CPPUNIT_ASSERT(http_request->body_file_len == -1);

  char *header_msg = createRequestMsg(*http_request);

  char expected[] =
    "POST /index.html?query=this_is_query HTTP/1.1\r\n"
    "HOST: localhost\r\n"
    "User-Agent: sample_http_client\r\n"
    "Accept: */*\r\n"
    "\r\n";

  CPPUNIT_ASSERT(strcmp(expected, header_msg) == 0);

  deleteHttpRequest(http_request);
}


void HeaderUtilTest::test_createRequestMsg_post_F_P_Q_H_B() {
  char method[] = "POST";
  char fqdn[] = "localhost";
  char path[] = "/index.html";
  char query[] = "?query=this_is_query";
  char user_agent[] = "User-Agent: sample_http_client";
  char accept[] = "Accept: */*";
  char body[] = "This is body data.";

  LinkedList *headers = initLinkedList();

  char *ua = copyStr(user_agent);
  addData(headers, ua);

  char *acc = copyStr(accept);
  addData(headers, acc);

  httpRequest *http_request = initHttpRequest(
    method,
    fqdn,
    path,
    query,
    headers,
    body,
    -1,     // body_file
    -1);    // body_file_len
  CPPUNIT_ASSERT(http_request != NULL);
  CPPUNIT_ASSERT(!strcmp(http_request->method, method));
  CPPUNIT_ASSERT(!strcmp(http_request->host, fqdn));
  CPPUNIT_ASSERT(!strcmp(http_request->path, path));
  CPPUNIT_ASSERT(!strcmp(http_request->query, query));
  CPPUNIT_ASSERT(http_request->headers->data_num == 2);
  CPPUNIT_ASSERT(!strcmp(http_request->body, body));
  CPPUNIT_ASSERT(http_request->body_file == -1);
  CPPUNIT_ASSERT(http_request->body_file_len == -1);

  char *header_msg = createRequestMsg(*http_request);

  char expected[BUF_SIZE];
  snprintf(
    expected,
    BUF_SIZE,
    "POST /index.html?query=this_is_query HTTP/1.1\r\n"
    "HOST: localhost\r\n"
    "User-Agent: sample_http_client\r\n"
    "Accept: */*\r\n"
    "Content-Length: %d\r\n"
    "\r\n"
    "%s",
    strlen(body), body);

  CPPUNIT_ASSERT(strcmp(expected, header_msg) == 0);

  deleteHttpRequest(http_request);
}


void HeaderUtilTest::test_createRequestMsg_post_F_P_Q_H_BF() {
  char method[] = "POST";
  char fqdn[] = "localhost";
  char path[] = "/index.html";
  char query[] = "?query=this_is_query";
  char user_agent[] = "User-Agent: sample_http_client";
  char accept[] = "Accept: */*";

  // create body data file
  char input_file[] = "./input_file.dat";
  int input_file_descriptor =
    open(input_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

  CPPUNIT_ASSERT(errno == 0);
  char message[] = "This is body data.";
  int write_size = write(input_file_descriptor, message, strlen(message));
  CPPUNIT_ASSERT(write_size == strlen(message));

  LinkedList *headers = initLinkedList();

  char *ua = copyStr(user_agent);
  addData(headers, ua);

  char *acc = copyStr(accept);
  addData(headers, acc);

  httpRequest *http_request = initHttpRequest(
    method,
    fqdn,
    path,
    query,
    headers,
    NULL,
    input_file_descriptor,
    strlen(message));
  CPPUNIT_ASSERT(http_request != NULL);
  CPPUNIT_ASSERT(!strcmp(http_request->method, method));
  CPPUNIT_ASSERT(!strcmp(http_request->host, fqdn));
  CPPUNIT_ASSERT(!strcmp(http_request->path, path));
  CPPUNIT_ASSERT(!strcmp(http_request->query, query));
  CPPUNIT_ASSERT(http_request->headers->data_num == 2);
  CPPUNIT_ASSERT(http_request->body == NULL);
  CPPUNIT_ASSERT(http_request->body_file == input_file_descriptor);
  CPPUNIT_ASSERT(http_request->body_file_len == strlen(message));

  char *header_msg = createRequestMsg(*http_request);

  char expected[BUF_SIZE];
  snprintf(
    expected,
    BUF_SIZE,
    "POST /index.html?query=this_is_query HTTP/1.1\r\n"
    "HOST: localhost\r\n"
    "User-Agent: sample_http_client\r\n"
    "Accept: */*\r\n"
    "Content-Length: %d\r\n"
    "\r\n",
    strlen(message));

  CPPUNIT_ASSERT(strcmp(expected, header_msg) == 0);

  close(input_file_descriptor);
  remove(input_file);
  deleteHttpRequest(http_request);
}


void HeaderUtilTest::test_parseResponseHdr_200() {
  char header[] = "HTTP/1.1 200 OK\r\n"
                  "Content-Length: 18\r\n\r\n";
  // parse http response header
  httpResponse *http_response = parseResponseHdr(header);
  CPPUNIT_ASSERT(200 == http_response->status);

  // get each header from http_response->headers
  LinkedListData *first_hdr = getData(http_response->headers, 0);
  LinkedListData *second_hdr = getData(http_response->headers, 1);
  CPPUNIT_ASSERT(strcmp("HTTP/1.1 200 OK", (char *)first_hdr->data) == 0);
  CPPUNIT_ASSERT(strcmp("Content-Length: 18", (char *)second_hdr->data) == 0);
}


void HeaderUtilTest::test_getHdrStat_200() {
  LinkedList *headers = initLinkedList();
  char header[] = "HTTP/1.1 200 OK";
  int header_len = sizeof(header);
  char *data = (char *) malloc(sizeof(char *) * (header_len + 1));
  snprintf(data, header_len + 1, header);
  addData(headers, data);
  int status = getHdrStat(headers);
  CPPUNIT_ASSERT(200 == status);

  deleteLinkedList(headers);
}


void HeaderUtilTest::test_getHdrStat_404() {
  LinkedList *headers = initLinkedList();
  char header[] = "HTTP/1.1 404 Not Found";
  int header_len = sizeof(header);
  char *data = (char *) malloc(sizeof(char *) * (header_len + 1));
  snprintf(data, header_len + 1, header);
  addData(headers, data);
  int status = getHdrStat(headers);
  CPPUNIT_ASSERT(404 == status);

  deleteLinkedList(headers);
}


void HeaderUtilTest::test_parseRequestHdr_get_noQ() {
  char header[] = "GET /index.html HTTP/1.1\r\n"
                  "User-Agent: curl/7.29.0\r\n"
                  "Host: localhost\r\n"
                  "Accept: */*\r\n\r\n";
  httpRequest *http_request = parseRequestHdr(header);
  LinkedListData *hdr0 = getData(http_request->headers, 0);
  CPPUNIT_ASSERT(strcmp("GET /index.html HTTP/1.1", (char *)(hdr0->data)) == 0);
  LinkedListData *hdr1 = getData(http_request->headers, 1);
  CPPUNIT_ASSERT(strcmp("User-Agent: curl/7.29.0", (char *)(hdr1->data)) == 0);
  LinkedListData *hdr2 = getData(http_request->headers, 2);
  CPPUNIT_ASSERT(strcmp("Host: localhost", (char *)(hdr2->data)) == 0);
  LinkedListData *hdr3 = getData(http_request->headers, 3);
  CPPUNIT_ASSERT(strcmp("Accept: */*", (char *)(hdr3->data)) == 0);
  CPPUNIT_ASSERT(strcmp("GET", http_request->method) == 0);
  CPPUNIT_ASSERT(strcmp("localhost", http_request->host) == 0);
  CPPUNIT_ASSERT(strcmp("/index.html", http_request->path) == 0);
  CPPUNIT_ASSERT(http_request->query == NULL);
}


void HeaderUtilTest::test_parseRequestHdr_get_Q() {
  char header[] = "GET /index.html?query=abc HTTP/1.1\r\n"
                  "User-Agent: curl/7.29.0\r\n"
                  "Host: localhost\r\n"
                  "Accept: */*\r\n\r\n";
  httpRequest *http_request = parseRequestHdr(header);
  LinkedListData *hdr0 = getData(http_request->headers, 0);
  CPPUNIT_ASSERT(strcmp("GET /index.html?query=abc HTTP/1.1", (char *)(hdr0->data)) == 0);
  LinkedListData *hdr1 = getData(http_request->headers, 1);
  CPPUNIT_ASSERT(strcmp("User-Agent: curl/7.29.0", (char *)(hdr1->data)) == 0);
  LinkedListData *hdr2 = getData(http_request->headers, 2);
  CPPUNIT_ASSERT(strcmp("Host: localhost", (char *)(hdr2->data)) == 0);
  LinkedListData *hdr3 = getData(http_request->headers, 3);
  CPPUNIT_ASSERT(strcmp("Accept: */*", (char *)(hdr3->data)) == 0);
  CPPUNIT_ASSERT(strcmp("GET", http_request->method) == 0);
  CPPUNIT_ASSERT(strcmp("localhost", http_request->host) == 0);
  CPPUNIT_ASSERT(strcmp("/index.html", http_request->path) == 0);
  CPPUNIT_ASSERT(strcmp("?query=abc", http_request->query) == 0);
}


void HeaderUtilTest::test_getHdrMethod() {
  LinkedList *headers = initLinkedList();
  char header[] = "GET /index.html HTTP/1.1";
  addData(headers, header);
  char *method = getHdrMethod(headers);
  CPPUNIT_ASSERT(strcmp("GET", method) == 0);
}


void HeaderUtilTest::test_getHdrPath_noQ() {
  LinkedList *headers = initLinkedList();
  char header[] = "GET /index.html HTTP/1.1";
  addData(headers, header);
  char *path = getHdrPath(headers);
  CPPUNIT_ASSERT(strcmp("/index.html", path) == 0);
}


void HeaderUtilTest::test_getHdrPath_Q() {
  LinkedList *headers = initLinkedList();
  char header[] = "GET /index.html?query=abc HTTP/1.1";
  addData(headers, header);
  char *path = getHdrPath(headers);
  CPPUNIT_ASSERT(strcmp("/index.html", path) == 0);
}


void HeaderUtilTest::test_getHdrQuery_noQ() {
  LinkedList *headers = initLinkedList();
  char header[] = "GET /index.html HTTP/1.1";
  addData(headers, header);
  char *query = getHdrQuery(headers);
  CPPUNIT_ASSERT(NULL == query);
}


void HeaderUtilTest::test_getHdrQuery_Q() {
  LinkedList *headers = initLinkedList();
  char header[] = "GET /index.html?query=abc HTTP/1.1";
  addData(headers, header);
  char *query = getHdrQuery(headers);
  CPPUNIT_ASSERT(strcmp("?query=abc", query) == 0);
}


void HeaderUtilTest::test_createResponseMsg_noB() {
  LinkedList *headers = initLinkedList();
  char cache_control[] = "Cache-Control: no-cache";
  addData(headers, cache_control);

  httpResponse *http_response = initHttpResponse(
    200,
    headers,
    NULL,
    -1,
    -1);

  char *msg = createResponseMsg(*http_response);

  char expected[BUF_SIZE];
  snprintf(
    expected,
    BUF_SIZE,
    "HTTP/1.1 200 OK\r\n"
    "Content-Length: 0\r\n"
    "%s\r\n"
    "\r\n",
    cache_control);

  CPPUNIT_ASSERT(strcmp(expected, msg) == 0);

  deleteHttpResponse(http_response);
}


void HeaderUtilTest::test_createResponseMsg_B() {
  LinkedList *headers = initLinkedList();
  char cache_control[] = "Cache-Control: no-cache";
  addData(headers, cache_control);
  char body[] = "This is body data.";

  httpResponse *http_response = initHttpResponse(
    200,
    headers,
    body,
    -1,
    -1);

  char *msg = createResponseMsg(*http_response);

  char expected[BUF_SIZE];
  snprintf(
    expected,
    BUF_SIZE,
    "HTTP/1.1 200 OK\r\n"
    "Content-Length: %d\r\n"
    "%s\r\n"
    "\r\n"
    "%s",
    strlen(body), cache_control, body);

  CPPUNIT_ASSERT(strcmp(expected, msg) == 0);

  deleteHttpResponse(http_response);
}


void HeaderUtilTest::test_createResponseMsg_BF() {
  // create body data file
  char input_file[] = "./input_file.dat";
  int input_file_descriptor =
    open(input_file, O_RDWR | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

  CPPUNIT_ASSERT(errno == 0);
  char body[] = "This is body data.";
  int write_size = write(input_file_descriptor, body, strlen(body));
  CPPUNIT_ASSERT(write_size == strlen(body));

  LinkedList *headers = initLinkedList();
  char cache_control[] = "Cache-Control: no-cache";
  addData(headers, cache_control);

  httpResponse *http_response = initHttpResponse(
    200,
    headers,
    NULL,
    input_file_descriptor,
    strlen(body));

  char *msg = createResponseMsg(*http_response);

  char expected[BUF_SIZE];
  snprintf(
    expected,
    BUF_SIZE,
    "HTTP/1.1 200 OK\r\n"
    "Content-Length: %d\r\n"
    "%s\r\n"
    "\r\n",
    strlen(body), cache_control);

  CPPUNIT_ASSERT(strcmp(expected, msg) == 0);

  close(input_file_descriptor);
  remove(input_file);
  deleteHttpResponse(http_response);
}
